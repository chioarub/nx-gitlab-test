import { Component } from '@angular/core';

@Component({
  selector: 'nx-gitlab-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nx-gitlab-test';
}
